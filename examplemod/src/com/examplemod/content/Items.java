package com.examplemod.content;

import com.badlogic.gdx.graphics.Color;
import io.anuke.mindustry.game.ContentList;
import io.anuke.mindustry.type.ContentType;
import io.anuke.mindustry.type.Item;

public class Items implements ContentList {
    public static Item exampleItemRaw, exampleItemRefined;
    @Override
    public void load() {
        exampleItemRaw = new Item("examplemod:item-exampleRaw", Color.valueOf("AAAAAA")){{
            explosiveness = 1f;
            //generate ore
            genOre = true;
            hardness = 1;
        }};

        exampleItemRefined = new Item("examplemod:item-exampleRefined", Color.valueOf("FFFFFF")){{
            explosiveness = 1f;
        }};
    }

    @Override
    public ContentType type() {
        return ContentType.item;
    }
}
