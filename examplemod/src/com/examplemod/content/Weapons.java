package com.examplemod.content;

import io.anuke.mindustry.content.AmmoTypes;
import io.anuke.mindustry.content.fx.ShootFx;
import io.anuke.mindustry.game.ContentList;
import io.anuke.mindustry.type.ContentType;
import io.anuke.mindustry.type.Weapon;
import io.anuke.ucore.core.Core;

public class Weapons implements ContentList {
    public static Weapon exampleGun;

    @Override
    public void load() {
        exampleGun = new Weapon("examplemod:examplegun") {
            {
            length = 1f;
            reload = 0.5f;
            roundrobin = true;
            shots = 600;
            inaccuracy = 150f;
            ammo = AmmoTypes.bulletMech;
            recoil = 60f;
            velocityRnd = 70f;
            ejectEffect = ShootFx.shellEjectSmall;
            }

            //sprites
            @Override
            public void load() {
                //Render sprite examplemod:error instead of examplemod:examplegun
                region = Core.atlas.getRegion("examplemod:error");
                equipRegion = Core.atlas.getRegion("examplemod:error");
            }
        };
    }

    @Override
    public ContentType type() {
        return ContentType.weapon;
    }
}
