package com.examplemod.content;

import com.badlogic.gdx.graphics.Color;
import io.anuke.mindustry.game.ContentList;
import io.anuke.mindustry.type.ContentType;
import io.anuke.mindustry.type.Mech;
import io.anuke.ucore.core.Core;

public class Mechs implements ContentList {
    public static Mech exampleMech;

    @Override
    public void load() {
        //create exampleMech
        exampleMech = new Mech("examplemod:error", false) {
            {
                drillPower = 4;
                speed = 0.63f;
                boostSpeed = 0.86f;
                itemCapacity = 50;
                armor = 30f;
                weaponOffsetX = -1;
                weaponOffsetY = -1;
                //set mech weapon as exampleGun
                weapon = Weapons.exampleGun;
                boostSpeed = 10f;
                maxSpeed = 14f;
                trailColor = Color.valueOf("d3ddff");
            }

            @Override
            public void load() {
                //use examplemod:error for every sprite as the mod does not have all required sprites
                if (!flying) {
                    legRegion = Core.atlas.getRegion(name);
                    baseRegion = Core.atlas.getRegion(name);
                }

                region = Core.atlas.getRegion(name);
                iconRegion = Core.atlas.getRegion(name);
            }
        };
    }

    @Override
    public ContentType type() {
        return null;
    }
}
