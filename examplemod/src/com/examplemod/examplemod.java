package com.examplemod;

import com.examplemod.content.*;
import com.examplemod.misc.ModConfig;
import com.examplemod.misc.ModMisc;
import com.examplemod.world.Worldgen;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.modloader.Annotations;
import io.anuke.mindustry.modloader.api.Config;

import java.util.Map;

//@Annotations.Mod tells the modloader that this class is main class
@Annotations.Mod
public class examplemod {
    //Config map object
    public static Map config;

    //Call on preInit, method can be called anything just has to have the annotation
    //before modules are initialized (Vars.control, Vars.world, Vars.ui, Vars.renderer etc)
    @Annotations.MMLEvent(event = Annotations.Event.preinit)
    public static void onPreInitialization() {
        //Check if config exists and assign to config variable, if not call ModConfig.createConfig()
        if(Config.configExist("example")) config = Config.getConfig("example");
        else ModConfig.createConfig();


        //add to content for loading
        Vars.content.content.add(new Items());
        Vars.content.content.add(new Weapons());
        Vars.content.content.add(new Mechs());
        Vars.content.content.add(new Blocks());
        Vars.content.content.add(new Recipes());
        //add worldgen to worldgen
        Vars.world.generator.worldgenObjects.add(new Worldgen());
    }

    //Call on init
    //after modules are initialized
    @Annotations.MMLEvent(event = Annotations.Event.init)
    public static void onInitialization() {
    }

    //Call on postInit
    //after everything
    @Annotations.MMLEvent(event = Annotations.Event.postinit)
    public static void onPostInitialization() {
        //Initialize miscellaneous
        ModMisc.onPostInitialization();
    }
}
