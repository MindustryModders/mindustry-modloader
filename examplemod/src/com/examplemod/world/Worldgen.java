package com.examplemod.world;

import com.badlogic.gdx.utils.Array;
import com.examplemod.content.Items;
import io.anuke.mindustry.maps.generation.WorldGenerator;
import io.anuke.mindustry.modloader.objects.WorldgenObject;
import io.anuke.mindustry.type.Item;

public class Worldgen implements WorldgenObject {

    @Override
    public Array<Item> confirmSectorOres(Array<Item> ores) {
        //add to ores
        ores.add(Items.exampleItemRaw);
        return ores;
    }
}
