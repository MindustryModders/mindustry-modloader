package com.examplemod.misc;

import com.examplemod.examplemod;
import io.anuke.mindustry.modloader.api.Config;

import java.util.HashMap;
import java.util.Map;

public class ModConfig {

    //Create config
    public static void createConfig() {
        //new HashMap for config
        Map map = new HashMap<>();
        //put defog boolean to config
        map.put("defog", false);
        //write config
        Config.writeConfig("example",map);
        //set examplemod.config to map
        examplemod.config = map;
    }

}
