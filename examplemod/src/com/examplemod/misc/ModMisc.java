package com.examplemod.misc;

import com.examplemod.examplemod;
import io.anuke.mindustry.Vars;

public class ModMisc {
    public static void onPostInitialization() {
        //Check defog disable fog if true
        if(Boolean.parseBoolean((String) examplemod.config.get("defog"))) Vars.showFog = false;
    }
}
