package io.anuke.mindustry.server;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.headless.HeadlessApplication;
import com.badlogic.gdx.backends.headless.HeadlessApplicationConfiguration;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import io.anuke.kryonet.KryoClient;
import io.anuke.kryonet.KryoServer;
import io.anuke.mindustry.core.Platform;
import io.anuke.mindustry.net.Net;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.util.EmptyLogger;

import java.io.File;
import java.net.URL;

public class ServerLauncher extends HeadlessApplication{

    public ServerLauncher(ApplicationListener listener, HeadlessApplicationConfiguration config){
        super(listener, config);

        //don't do anything at all for GDX logging: don't want controller info and such
        Gdx.app.setApplicationLogger(new EmptyLogger());
    }

    public static void main(String[] args){
        try{

            Net.setClientProvider(new KryoClient());
            Net.setServerProvider(new KryoServer());

            HeadlessApplicationConfiguration config = new HeadlessApplicationConfiguration();
            Settings.setPrefHandler((appName) -> Gdx.files.local("config"));

            Platform.instance = new Platform() {

                private final GroovyClassLoader loader = new GroovyClassLoader();

                @Override
                public void addURLs(URL... url) {
                    try {
                        for (URL u : url) {
                            loader.addURL(u);
                        }
                    } catch (Throwable t) {
                        System.out.println("Oh no, we died");
                        t.printStackTrace();
                    }
                }

                @Override
                public ClassLoader getClassloader() {
                    return loader;
                }

                @Override
                public Class[] getClasses() {
                    return loader.getLoadedClasses();
                }

                GroovyShell sh = new GroovyShell(loader);

                @Override
                public Object runScript(File file) {
                    try {
                        return sh.run(file, new String[]{});
                    } catch (Exception e) {
                        e.printStackTrace();
                        return e;
                    }
                }
            };

            new ServerLauncher(new MindustryServer(args), config);
        }catch(Throwable t){
            CrashHandler.handle(t);
        }

        //find and handle uncaught exceptions in libGDX thread
        for(Thread thread : Thread.getAllStackTraces().keySet()){
            if(thread.getName().equals("HeadlessApplication")){
                thread.setUncaughtExceptionHandler((t, throwable) -> {
                    CrashHandler.handle(throwable);
                    System.exit(-1);
                });
                break;
            }
        }
    }
}