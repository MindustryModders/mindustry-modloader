package io.anuke.mindustry.modloader;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.ucore.core.Core;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class SpriteLoader {
    public static void loadSprites(Mod mod, Array<ZipEntry> entries, ZipFile zf) {
        try {
            for (ZipEntry ze:entries) {
                InputStream input = zf.getInputStream(ze);
                String[] sray = ze.getName().replace(".png", "").split("/");
                Pixmap p = new Pixmap(new Gdx2DPixmap(input, 0));
                if (sray[sray.length-1].equals("icon")) mod.icon = new TextureRegion(new Texture(p));
                else ModloaderVars.packer.pack(mod.modInfo.id + ":" + sray[sray.length-1], p);
            }

            Logger.print("Loaded Atlas for mod: {0}", mod.modInfo.name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
