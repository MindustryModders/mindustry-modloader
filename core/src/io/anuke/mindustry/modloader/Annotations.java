package io.anuke.mindustry.modloader;

import java.lang.annotation.*;


public class Annotations {
    public enum Event{preinit, init, postinit}

    @Documented
    @Target(ElementType.METHOD)
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    public @interface MMLEvent{
        Event event();
    }

    @Documented
    @Target(ElementType.TYPE)
    @Inherited
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Mod{ }
}
