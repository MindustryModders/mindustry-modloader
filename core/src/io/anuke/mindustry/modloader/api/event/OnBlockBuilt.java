package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.game.Team;
import io.anuke.mindustry.modloader.api.Event;
import io.anuke.mindustry.world.Block;
import io.anuke.mindustry.world.Tile;

public class OnBlockBuilt extends Event {

    public Tile tile;
    public Block block;
    public int builderID;
    public byte rotation;
    public Team team;

    public OnBlockBuilt(Tile tile, Block block, int builderID, byte rotation, Team team) {
        super("onBlockBuilt");
        this.tile = tile;
        this.block = block;
        this.builderID = builderID;
        this.rotation = rotation;
        this.team = team;
    }
}
