package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.entities.Player;
import io.anuke.mindustry.modloader.api.Event;

public class OnPlayerDeath extends Event {
    public Player player;

    public OnPlayerDeath(Player player) {
        super("OnPlayerDeath");
        this.player = player;
    }
}
