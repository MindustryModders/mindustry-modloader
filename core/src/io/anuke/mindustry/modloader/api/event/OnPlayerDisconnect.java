package io.anuke.mindustry.modloader.api.event;

import io.anuke.mindustry.entities.Player;
import io.anuke.mindustry.modloader.api.Event;

public class OnPlayerDisconnect extends Event {

    //Could be null
    public Player player;

    public OnPlayerDisconnect(Player player) {
        super("onPlayerDisconnect");
        this.player = player;
    }
}
