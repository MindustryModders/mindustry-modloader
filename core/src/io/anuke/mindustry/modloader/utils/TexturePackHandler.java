package io.anuke.mindustry.modloader.utils;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.modloader.ModloaderVars;
import io.anuke.mindustry.modloader.objects.TexturePack;
import io.anuke.mindustry.modloader.objects.TexturePackContainer;
import io.anuke.mindustry.modloader.objects.TexturePackInfo;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;

import java.io.*;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class TexturePackHandler {

    public static TexturePackContainer container = new TexturePackContainer();

    public static void loadAllTexturePacks() {
        for (TexturePack tx : container.getTexturePacks()) {
            if (tx.enabled)
                loadTexturePack(tx);
        }
    }

    public static void removeTexturePack(TexturePack texturePack) {
        container.removeTexturePack(texturePack);
    }

    public static void registerTexturePacks() {
        Array<String> order = Settings.getObject("orderTexturepacks", Array.class, Array::new);
        Array<String> enabled = Settings.getObject("enabledTexturepacks", Array.class, Array::new);

        container = new TexturePackContainer();
        TexturePackContainer presort = new TexturePackContainer();

        for (FileHandle file : Vars.texturePackDirectory.list()) {
            TexturePack tp = new TexturePack();
            tp.file = file.file();
            tp.info = loadMeta(file);
            if (tp.info == null) {
                Logger.warn("Failed loading texturepack file " + file.name() + "!");
                continue;
            }
            tp.enabled = enabled.contains(tp.info.name, false);
            presort.addTexturePack(tp);
        }

        for (String or : order) {
            for (TexturePack tx : presort.getTexturePacks()) {
                if (!tx.info.name.equals(or))
                    continue;
                container.addTexturePack(tx);
                Logger.info("Added texturepack " + tx.info.name);
                presort.removeTexturePack(tx);
            }
        }

        for (TexturePack tx : presort.getTexturePacks()) {
            container.addTexturePack(tx);
            presort.removeTexturePack(tx);
        }
    }

    public static void save() {
        Array<String> enabled = new Array<String>();
        Array<String> order = new Array<String>();

        for (TexturePack tx : TexturePackHandler.container.getTexturePacks()) {
            if (tx.enabled)
                enabled.add(tx.info.name);
            order.add(tx.info.name);
        }
        Settings.putObject("orderTexturepacks", order);
        Settings.putObject("enabledTexturepacks", enabled);
        Settings.save();
    }

    private static TexturePackInfo loadMeta(FileHandle meta) {
        TexturePackInfo tpinfo = null;
        if (meta.extension().equals("zip")) {
            try {
                ZipFile zipFile = new ZipFile(meta.file());
                InputStream input = zipFile.getInputStream(zipFile.getEntry("texturepackmeta"));

                YamlReader yr = new YamlReader(new InputStreamReader(input));
                tpinfo = yr.read(TexturePackInfo.class);

                yr.close();
                zipFile.close();
            } catch (Exception e) {
                Logger.err("Error in texture pack metadata loader, zip.");
                e.printStackTrace();
            }
        } else if (meta.isDirectory()) {
            meta = meta.child("texturepackmeta");
            if (meta.exists()) {
                try {
                    YamlReader yr = new YamlReader(meta.reader());
                    tpinfo = yr.read(TexturePackInfo.class);
                    yr.close();
                } catch (Exception e) {
                    Logger.err("Error in texture pack metadata loader, folder.");
                }
            }
        }

        return tpinfo;
    }

    public static boolean loadTexturePack(TexturePack texturePack) {

        File file = texturePack.file;

        if (file.getName().endsWith(".zip")) {
            try {
                ZipFile zipFile = new ZipFile(file);

                Enumeration<? extends ZipEntry> entries = zipFile.entries();

                while (entries.hasMoreElements()) {
                    ZipEntry entry = entries.nextElement();
                    if (entry.getName().endsWith(".png")) {
                        InputStream input = zipFile.getInputStream(entry);
                        String[] sray = entry.getName().replace(".png", "").split("/");
                        Pixmap p = new Pixmap(new Gdx2DPixmap(input, 0));

                        if (entry.getName().startsWith("icon")) {
                            Texture t = new Texture(p);
                            TextureData td = t.getTextureData();
                            texturePack.icon = new TextureRegion(t, 0, 0, td.getWidth(), td.getHeight());
                            continue;
                        }

                        if (sray[0].equals("mindustry")) {
                            ModloaderVars.packer.pack(sray[sray.length - 1], p);
                        } else {
                            ModloaderVars.packer.pack(sray[0] + ":" + sray[sray.length - 1], p);
                        }

                        input.close();
                    }
                }

                zipFile.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (file.isDirectory()) {
            Utils.fileWalkerDo(file, true, TexturePackHandler::replaceTexture);
        } else {
            Logger.warn("Texture packs that are not folders or zip files wont be loaded.");
            return false;
        }

        return true;
    }

    private static void replaceTexture(File file) {
        if (!file.toString().endsWith(".png")) return;
        try {
            FileHandle f = new FileHandle(file);
            Pixmap p = new Pixmap(f);
            String[] pathCut = f.path().toLowerCase().split("/");

            if (pathCut[pathCut.length - 2].equals("mindustry"))
                ModloaderVars.packer.pack(f.nameWithoutExtension(), p);
            else
                ModloaderVars.packer.pack(pathCut[pathCut.length - 2] + ":" + f.nameWithoutExtension(), p);
        } catch (GdxRuntimeException ex) {
            Logger.warn("Couldn't load file: {0}", file.toString());
        }
    }
}
