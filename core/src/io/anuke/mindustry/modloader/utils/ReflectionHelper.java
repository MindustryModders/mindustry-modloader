package io.anuke.mindustry.modloader.utils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ReflectionHelper {
    public static Field getField(Class clazz, String fieldName) throws NoSuchFieldException {
        try {
            return clazz.getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) throw e;
            else return getField(superClass, fieldName);
        }
    }

    public static boolean setField(Class clazz, String fieldName, Object setAs) throws NoSuchFieldException, IllegalAccessException {
        try {
            try {
                Field f = clazz.getDeclaredField(fieldName);
                makeFieldAccessible(f);
                f.set(clazz, setAs);
                return true;
            } catch (NoSuchFieldException e) {
                Class superClass = clazz.getSuperclass();
                if (superClass == null) throw e;
                else return setField(superClass, fieldName, setAs);
            }
        } catch (IllegalAccessException e) {
            throw e;
        }
    }

    public static void makeFieldAccessible(Field field) {
        if (!Modifier.isPublic(field.getModifiers()) ||
                !Modifier.isPublic(field.getDeclaringClass().getModifiers()))
            field.setAccessible(true);
    }

    public static Object invokeMethod(Class clazz, Object object, String methodName, Class<?>[] parameterTypes, Object[] parameters) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        try {
            Method method = clazz.getDeclaredMethod(methodName, parameterTypes);
            makeMethodAccessible(method);
            return method.invoke(object, parameters);
        } catch (Throwable e) {
            Class superClass = clazz.getSuperclass();
            if (superClass == null) throw e;
            else return invokeMethod(superClass,  object, methodName, parameterTypes, parameters);
        }
    }

    public static void makeMethodAccessible(Method method) {
        if (!Modifier.isPublic(method.getModifiers()) ||
                !Modifier.isPublic(method.getDeclaringClass().getModifiers()))
            method.setAccessible(true);
    }
}
