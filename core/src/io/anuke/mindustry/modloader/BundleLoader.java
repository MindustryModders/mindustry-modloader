package io.anuke.mindustry.modloader;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.utils.BundleHelper;
import io.anuke.mindustry.modloader.utils.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class BundleLoader {
    public static void loadBundles(Mod mod, Array<ZipEntry> entries, ZipFile zf) {
        try {
            Locale loc = BundleHelper.getLocale();
            String locUnderscore = loc.toLanguageTag().replace("-", "_");
            ZipEntry defBundle = null;

            for (ZipEntry ze : entries) {
                if (loc.toLanguageTag().equals("en-US") && ze.getName().endsWith("bundle.properties")) {
                    ObjectMap<String, String> map = BundleHelper.bundleBuilder(new BufferedReader(new InputStreamReader(zf.getInputStream(ze))));
                    BundleHelper.putAll(map);
                    for (String s:map.keys()) {
                        Logger.info("{0}: {1}", s, map.get(s));
                    }
                    Logger.info("Loaded en_US bundle for mod {0}", mod.modInfo.name);
                    return;
                } else if (!loc.toLanguageTag().equals("en-US")) {
                    if (ze.getName().contains(locUnderscore)) {
                        BundleHelper.putAll(BundleHelper.bundleBuilder(new BufferedReader(new InputStreamReader(zf.getInputStream(ze)))));
                        Logger.info("Loaded bundle {1} for mod {0}", mod.modInfo.name, locUnderscore);
                        return;
                    }
                }
                if (ze.getName().endsWith("bundle.properties"))
                    defBundle = ze;
            }

            if(defBundle != null) {
                BundleHelper.putAll(BundleHelper.bundleBuilder(new BufferedReader(new InputStreamReader(zf.getInputStream(defBundle)))));
                Logger.info("Loaded default bundle for mod {0}", mod.modInfo.name);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}