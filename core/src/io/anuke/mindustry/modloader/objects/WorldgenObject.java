package io.anuke.mindustry.modloader.objects;

import com.badlogic.gdx.utils.Array;
import io.anuke.mindustry.maps.Sector;
import io.anuke.mindustry.maps.generation.WorldGenerator;
import io.anuke.mindustry.type.Item;
import io.anuke.mindustry.world.Tile;

public interface WorldgenObject {
    default void preGenerateTile(Tile[][] tiles, Sector sector){}
    default void preElevation(Tile[][] tiles, Sector sector){}
    default void preCliffs(Tile[][] tiles, Sector sector){}
    default void postCliffs(Tile[][] tiles, Sector sector){}
    default void finalAdjustments(Tile[][] tiles, Sector sector){}

    default Array<Item> confirmSectorOres(Array<Item> ores){return ores;}
}
