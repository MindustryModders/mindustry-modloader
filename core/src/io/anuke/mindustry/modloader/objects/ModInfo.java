package io.anuke.mindustry.modloader.objects;

public class ModInfo {
    public ModInfo() {
    }

    public String id;
    public String name;
    public String author;
    public String version;
    public String desc;
    public String website;

    public boolean clientOnly = false;

    public String[] dependencies;
}
