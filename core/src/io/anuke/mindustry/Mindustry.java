package io.anuke.mindustry;

import io.anuke.mindustry.core.*;
import io.anuke.mindustry.game.EventType.GameLoadEvent;
import io.anuke.mindustry.io.BundleLoader;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.mindustry.modloader.utils.ProblemHandler;
import io.anuke.mindustry.modloader.utils.TexturePackHandler;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.core.Events;
import io.anuke.ucore.core.Timers;
import io.anuke.ucore.modules.ModuleCore;
import io.anuke.ucore.util.Log;

import static io.anuke.mindustry.Vars.*;

public class Mindustry extends ModuleCore{

    @Override
    public void init(){

        Timers.mark();

        Vars.init();

        Thread.setDefaultUncaughtExceptionHandler(new ProblemHandler());

        Log.setUseColors(false);

        Settings.load(Vars.appName, headless ? "io.anuke.mindustry.modloader.server" : "io.anuke.mindustry.modloader");
        BundleLoader.load();

        module(new Modloader());

        TexturePackHandler.registerTexturePacks();

        content.load();

        Vars.modloader.modInit();

        module(logic = new Logic());
        module(world = new World());
        module(control = new Control());
        module(renderer = new Renderer());
        module(ui = new UI());
        module(netServer = new NetServer());
        module(netClient = new NetClient());

        Logger.info(Core.skin.getDrawable("button").getClass());
    }

    @Override
    public void postInit(){
        Vars.modloader.modPostInit();
        Log.info("Time to load [total]: {0}", Timers.elapsed());
        Events.fire(new GameLoadEvent());
    }

    @Override
    public void render(){
        threads.handleBeginRender();
        super.render();
        threads.handleEndRender();
    }

}
