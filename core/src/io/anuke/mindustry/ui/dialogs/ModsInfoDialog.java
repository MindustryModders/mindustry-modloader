package io.anuke.mindustry.ui.dialogs;

import com.badlogic.gdx.Input.Keys;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.core.GameState.State;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.objects.ModInfo;
import io.anuke.mindustry.net.Net;
import io.anuke.ucore.scene.style.Drawable;
import io.anuke.ucore.scene.ui.ScrollPane;
import io.anuke.ucore.scene.ui.layout.Table;
import io.anuke.ucore.util.Bundles;

import static io.anuke.mindustry.Vars.*;

public class ModsInfoDialog extends FloatingDialog{
    public Table table;
    private Mod target;

    public ModsInfoDialog(){
        super("$text.mods.info");
        table = new Table("button");

        keyDown(key -> {
            if(key == Keys.ESCAPE || key == Keys.BACK) {
                hide();
            }
        });
    }

    public void show(Mod mod){
        target = mod;
        setup();
        show();
    }

    void setup(){
        content().clear();
        table.clear();
        Table c = new Table("button");
        ScrollPane pane = new ScrollPane(c);
        pane.setFadeScrollBars(false);
        pane.setScrollingDisabled(true, false);


        table.label(() -> target.modInfo.name + " " + target.modInfo.version).get().setFontScale(3f);
        table.row();
        table.label(() -> target.modInfo.author).get().setFontScale(1.5f);
        table.row();
        c.labelWrap(() -> Bundles.get(target.modInfo.clientOnly ? "text.mods.clientside" : "text.mods.notclientside")).left();
        c.row();
        c.labelWrap("");
        c.row();
        c.labelWrap(() -> {
            return Bundles.get("text.mods.desc") + " " + (target.modInfo.desc == null ? "Unknown" : target.modInfo.desc);
        }).left().growX();

        c.row();
        c.labelWrap("");
        c.row();

        if(target.modInfo.dependencies.length > 0) {
            c.labelWrap(Bundles.get("text.mods.dependencies") + " ").left();
            for (String s : target.modInfo.dependencies) {
                c.labelWrap("  " + s + ",").left();
                c.row();
            }
        }else{
            c.labelWrap(Bundles.get("text.mods.nodependencies")).left();
            c.row();
        }
        c.row();



        Table buttons = new Table();
        buttons.addImageTextButton("$text.back", "icon-arrow-left", 32f, () -> hide()).width(160f);
        buttons.addImageTextButton("$text.mods.webpage", "icon-egg", 32f, () -> ui.link.show(target.modInfo.website)).width(160f).disabled(target.modInfo.website == null);
        buttons.addImageTextButton("$text.mods.config", "icon-gear", 32f, () -> ui.config.setup(target.uuid)).width(160f);
        buttons.addImageTextButton("$text.delete", "icon-trash", 32f, () -> {
            ui.showConfirm("$text.confirm", "$text.mods.delete.confirm", () -> {
                hide();
                Vars.modloader.disableMod(target.uuid);
                Vars.modloader.delmod(target);
                ui.mods.setup();
            });
        }).width(160f);
        table.add(pane).growX();
        table.row();
        table.add(buttons);
        content().add(table);
    }
}
