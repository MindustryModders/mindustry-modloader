package io.anuke.mindustry.ui.dialogs;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Base64Coder;
import com.esotericsoftware.yamlbeans.YamlReader;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.core.Platform;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.objects.ModInfo;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.core.Timers;
import io.anuke.ucore.graphics.Draw;
import io.anuke.ucore.scene.ui.Label;
import io.anuke.ucore.scene.ui.ScrollPane;
import io.anuke.ucore.scene.ui.layout.Table;
import io.anuke.ucore.util.Bundles;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static io.anuke.mindustry.Vars.*;

public class ModsDialog extends FloatingDialog {

    ScrollPane pane;
    private Table slots;

    public ModsDialog() {
        this("$text.mods");
    }

    public ModsDialog(String title) {
        super(title);
        setup();

        shown(() -> {
            setup();
            Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));
        });

        addCloseButton();
    }

    protected void setup() {
        content().clear();

        slots = new Table();
        pane = new ScrollPane(slots);
        pane.setFadeScrollBars(false);
        pane.setScrollingDisabled(true, false);

        slots.marginRight(24);

        Timers.runTask(2f, () -> Core.scene.setScrollFocus(pane));

        for (Mod slot : Vars.modloader.getMods()) {
            Table txtbutton = new Table("pane");
            txtbutton.add("[accent]" + slot.modInfo.name == null ? "Unknown" : slot.modInfo.name).left().height(20);
            txtbutton.row();

            txtbutton.addImage(slot.icon == null ? Draw.region("error") : slot.icon).size(64, 64).left().expand();

            txtbutton.add(String.format(Bundles.format("text.editor.author") + " %s",
                    "[lightgray]" + (slot.modInfo.author == null ? "Unknown" : slot.modInfo.author))).left().top().pad(0);

            Table t = new Table();
            t.addImageButton(slot.enabled ? "icon-enabled" : "icon-disabled", "emptytoggle", 14 * 3, () -> {
                slot.enabled = !slot.enabled;
                if (slot.enabled)
                    Vars.modloader.enabledMods.add(slot.uuid);
                else
                    Vars.modloader.enabledMods.removeValue(slot.uuid, false);
                setup();
            }).checked(slot.enabled).right();

            t.addImageButton("icon-info", "empty", 14 * 3, () -> {
                ui.modsinfo.show(slot);
            });

            txtbutton.add(t).right();

            Table l = new Table();
            l.defaults().size(42f).left();
            l.addImageButton("icon-arrow-up", "empty", 14 * 3, () -> {
                Vars.modloader.swapModOrder(slot, "up");
                setup();
            });
            l.row();
            l.addImageButton("icon-arrow-down", "empty", 14 * 3, () -> {
                Vars.modloader.swapModOrder(slot, "down");
                setup();
            });

            slots.add(txtbutton).left().uniformX().fillX().pad(4).padRight(-4).margin(10f).marginLeft(20f).marginRight(20f).growX();
            slots.add(l).left().pad(4).margin(10f).marginLeft(20f);
            slots.row();
        }

        for (FileHandle slot : Vars.modloader.getScripts()) {
            String name = slot.nameWithoutExtension();
            Table txtbutton = new Table("pane");
            txtbutton.add("[accent]" + name).left().height(20).left();
            txtbutton.row();

            txtbutton.addImage(Draw.region("icon-gscript")).size(64, 64).left().expand();

            Table t = new Table();
            t.defaults().size(42f).right();
            t.right();
            t.addImageButton(Vars.modloader.isScriptEnabled(name) ? "icon-enabled" : "icon-disabled", "emptytoggle", 14 * 3, () -> {
                Vars.modloader.setScriptState(name, !Vars.modloader.isScriptEnabled(name));
                setup();
            }).checked(Vars.modloader.isScriptEnabled(name));
            t.addImageButton("icon-trash", "empty", 14 * 3, () ->
                    ui.showConfirm("$text.confirm", "$text.mods.delete.confirm", () -> {
                        Vars.modloader.setScriptState(slot.nameWithoutExtension(), false);
                        Vars.modloader.delScript(slot);
                        setup();
                    })
            );
            txtbutton.add(t).right();

            txtbutton.row();

            slots.add(txtbutton).left().uniformX().fillX().pad(4).padRight(-4).margin(10f).marginLeft(20f).marginRight(20f).growX();

            slots.row();
        }

        content().add(pane);
        content().row();
        addSetup();
    }

    private void addSetup() {
        if (Vars.modloader.getMods().size == 0 && Vars.modloader.getScripts().size == 0) {
            Table t = new Table("pane");
            Label l = new Label("$text.mods.none");
            t.add(l);
            slots.add(t.center());
        }

        slots.row();

        if (ios) return;

        Table bottom = new Table();

        bottom.addImageTextButton("$text.mods.apply", "icon-check", 14 * 3, () ->
                ui.showConfirm("$text.confirm", "$text.mods.apply.confirm", () -> {
                    Settings.putObject("enabledScripts", Vars.modloader.enabledScripts);
                    Settings.putObject("enabledMods", Vars.modloader.enabledMods);
                    Settings.save();
                    setup();
                })).fillX().margin(10f).minWidth(300f).height(70f).pad(4f).padRight(-4);

        bottom.addImageTextButton("$text.mods.import", "icon-add", 14 * 3, () ->
                Platform.instance.showFileChooser(Bundles.get("text.mods.import"), "Mod Jar", file -> {
                    try {
                        if (!file.toString().equals(Vars.modDirectory + "/" + file.name())) {
                            file.copyTo(Vars.modDirectory);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (file.extension().equals("groovy")) {
                        Vars.modloader.addScriptToContainer(file);
                        ui.showInfo("$text.mods.import.success");
                        setup();
                    } else {
                        try {
                            Mod mod = new Mod();
                            ZipFile zf = new ZipFile(mod.file);
                            ZipEntry ze = zf.getEntry("modmeta");

                            if (ze != null) {
                                InputStream input = zf.getInputStream(ze);

                                YamlReader yr = new YamlReader(new InputStreamReader(input, Charset.forName("UTF-8")));
                                mod.modInfo = yr.read(ModInfo.class);

                                if (mod.modInfo.id == null) mod.modInfo.id = mod.modInfo.name;
                                mod.uuid = Base64Coder.encodeString(mod.modInfo.author + mod.modInfo.name);

                                modloader.addModToContainer(mod);
                                ui.showInfo("$text.mods.import.success");
                                setup();
                                zf.close();
                            } else {
                                ui.showError("$text.mods.import.notMod");
                                file.delete();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, true, "jar", "groovy")).fillX().margin(10f).minWidth(300f).height(70f).pad(4f).padRight(-4);

        content().add(bottom);
    }
}