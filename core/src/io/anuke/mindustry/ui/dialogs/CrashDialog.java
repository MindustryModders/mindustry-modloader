package io.anuke.mindustry.ui.dialogs;

import com.badlogic.gdx.Input;
import io.anuke.mindustry.core.Platform;
import io.anuke.ucore.scene.ui.layout.Table;

public class CrashDialog extends FloatingDialog{

    private String log = "";
    private boolean fatal;
    private Table table;

    public CrashDialog(){
        super("$text.crashdialog.title");

        setup();
    }

    void setup(){
        content().clear();
        buttons().clear();

        content().add(log).left().minWidth(650f).wrap();

        buttons().addImageTextButton("$text.copylog", "icon-arrow-right", 30f, this::copyLog).size(230f, 64f);
        buttons().addImageTextButton("$text.back", "icon-arrow-left", 30f, this::close).size(230f, 64f);

        keyDown(key -> {
            if(key == Input.Keys.ESCAPE || key == Input.Keys.BACK)
                close();
        });
    }

    void close() {
        if(fatal) System.exit(1);
        else hide();
    }

    public void setLog(String log, boolean fatal) {
        this.log = log;
        this.fatal = fatal;

        setup();
    }

    void copyLog() {
        Platform.instance.setClipboard(log);
    }
}
