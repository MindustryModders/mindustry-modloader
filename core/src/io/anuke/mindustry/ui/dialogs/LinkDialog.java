package io.anuke.mindustry.ui.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import io.anuke.mindustry.graphics.Palette;
import io.anuke.ucore.scene.ui.Dialog;
import io.anuke.ucore.scene.ui.layout.Table;

import static io.anuke.mindustry.Vars.ui;

public class LinkDialog extends FloatingDialog{
    private String url;
    public LinkDialog(){
        super("");
        keyDown(key -> {
            if(key == Input.Keys.ESCAPE || key == Input.Keys.BACK) {
                hide();
            }
        });
    }

    void setup(){
        content().clear();
        content().margin(12f);
        Table t = new Table("button");
        t.defaults().size(170f, 50);
        t.label(() -> url).fillX();
        t.row();
        t.addButton("$text.back", this::hide);
        t.addButton("$text.copylink", () -> {
            Gdx.app.getClipboard().setContents(url);
        });
        t.addButton("$text.openlink", () -> {
            if(!Gdx.net.openURI(url)){
                ui.showError("$text.linkfail");
                Gdx.app.getClipboard().setContents(url);
            }
        });
        content().add(t);
    }

    public void show(String url){
        this.url = url;
        setup();
        show();
    }
}
