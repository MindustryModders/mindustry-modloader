package io.anuke.mindustry.core;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Base64Coder;
import com.esotericsoftware.yamlbeans.YamlReader;
import io.anuke.mindustry.Vars;
import io.anuke.mindustry.modloader.Annotations;
import io.anuke.mindustry.modloader.BundleLoader;
import io.anuke.mindustry.modloader.SpriteLoader;
import io.anuke.mindustry.modloader.api.Hooks;
import io.anuke.mindustry.modloader.objects.Mod;
import io.anuke.mindustry.modloader.objects.ModContainer;
import io.anuke.mindustry.modloader.objects.ModInfo;
import io.anuke.mindustry.modloader.utils.Logger;
import io.anuke.ucore.core.Core;
import io.anuke.ucore.core.Settings;
import io.anuke.ucore.graphics.Draw;
import io.anuke.ucore.modules.Module;
import io.anuke.ucore.util.Atlas;
import io.anuke.ucore.util.Log;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Modloader extends Module {

    public Array<String> enabledMods;
    public Array<String> enabledScripts;

    private ModContainer modContainer = new ModContainer();
    private Array<FileHandle> scriptFiles = new Array<>();
    private Class[] modClasses;
    private Array<String> dependencies = new Array<>();

    public Modloader() {
        enabledScripts = Settings.getObject("enabledScripts", Array.class, Array::new);
        enabledMods = Settings.getObject("enabledMods", Array.class, Array::new);

        if (!Vars.headless) {
            Core.atlas = new Atlas("sprites.atlas");
            Core.atlas.setErrorRegion("error");
        }

        Vars.modloader = this;

        loadScripts();
        loadMods();
        checkForDeleted();
    }

    private void checkForDeleted() {
        boolean found = false;
        for(String script : enabledScripts) {
            for (FileHandle fh:scriptFiles) {
                if(fh.nameWithoutExtension().equals(script)) {
                    found = true;
                    break;
                }
            }

            if(!found)
                enabledScripts.removeValue(script, false);

            found = false;
        }

        for(String mod : enabledMods) {
            for (Mod m : modContainer.getMods()) {
                if(m.uuid.equals(mod)) {
                    found = true;
                    break;
                }
            }

            if(!found)
                enabledMods.removeValue(mod, false);

            found = false;
        }

        Settings.putObject("enabledScripts", enabledScripts);
        Settings.putObject("enabledMods", enabledMods);
        Settings.save();
    }

    private void loadScripts() {
        for (FileHandle fileHandle : Vars.modDirectory.list()) {
            if (!fileHandle.extension().equals("groovy")) continue;
            scriptFiles.add(fileHandle);
        }
    }

    private ArrayList<URL> urls = new ArrayList<>();

    private void loadMods() {
        Hooks.initializeRenderHookMap();

        for (FileHandle fileHandle : Vars.modDirectory.list()) {
            //Skip files that can't be mods or libraries.
            if (!fileHandle.extension().equals("jar"))
                if (!fileHandle.isDirectory())
                    if (!fileHandle.extension().equals("zip"))
                        continue;

            Log.info("Loading mod {0}", fileHandle.nameWithoutExtension());
            File modFile = fileHandle.file();
            Mod mod = new Mod();
            mod.file = modFile;

            boolean library = false;
            if (!fileHandle.isDirectory()) {
                try {
                    ZipFile zf = new ZipFile(mod.file);
                    ZipEntry ze = zf.getEntry("modmeta");

                    if (ze != null) {
                        InputStream input = zf.getInputStream(ze);

                        YamlReader yr = new YamlReader(new InputStreamReader(input, Charset.forName("UTF-8")));
                        mod.modInfo = yr.read(ModInfo.class);
                        yr.close();
                        if (mod.modInfo.id == null) mod.modInfo.id = mod.modInfo.name;
                        mod.uuid = Base64Coder.encodeString(mod.modInfo.author + mod.modInfo.name);
                    } else {
                        Logger.warn("File {0} missing modmeta, presuming library", fileHandle.nameWithoutExtension());
                        library = true;
                    }

                    zf.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            } else if (fileHandle.child("modmeta").exists()) {
                try {
                    YamlReader yr = new YamlReader(fileHandle.child("modmeta").reader());
                    mod.modInfo = yr.read(ModInfo.class);
                    yr.close();
                    mod.uuid = Base64Coder.encodeString(mod.modInfo.author + mod.modInfo.name);
                } catch (Exception e) {
                    System.out.println("aeeeeeeeeeeeeeeeeeeeee");
                    e.printStackTrace();
                }
            } else {
                Logger.info("Folder found that does not have modmeta file");
                continue;
            }

            if (library) {
                try {
                    urls.add(modFile.toURI().toURL());
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }

            //Duplication check
            boolean duplicate = false;
            for (Mod m : modContainer.getMods()) {
                if (m.uuid.equals(mod.uuid)) {
                    Logger.err("Duplicate mod, not loading");
                    duplicate = true;
                    break;
                }
            }
            if (duplicate) continue;

            if (!enabledMods.contains(mod.uuid, false) || (Vars.headless && mod.modInfo.clientOnly)) {
                // <aaaaaa>

                try {
                    URI icURI = new URI("jar:" + fileHandle.file().toURI().toString());
                    ZipFile zipFile;

                    if (icURI.getScheme().equals("jar")) {
                        zipFile = new ZipFile(mod.file);
                        Enumeration<? extends ZipEntry> entries = zipFile.entries();
                        while (entries.hasMoreElements()) {
                            ZipEntry entry = entries.nextElement();
                            if (entry.getName().equals("resources/sprites/icon.png")) {
                                InputStream input = zipFile.getInputStream(entry);
                                String[] sray = entry.getName().replace(".png", "").split("/");
                                Pixmap p = new Pixmap(new Gdx2DPixmap(input, 0));
                                if (sray[sray.length - 1].equals("icon")) mod.icon = new TextureRegion(new Texture(p));
                            }
                        }
                        zipFile.close();
                    }

                } catch (Exception ignored) {
                }

                // </aaaaaa>
                if (!addModToContainer(mod)) Logger.warn(mod.modInfo.name + " already in container");
                continue;
            }

            mod.enabled = true;

            try {
                if (modFile != null)
                    urls.add(modFile.toURI().toURL());
                else continue;
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            try {
                if (!Vars.headless) {
                    mod.icon = Draw.getBlankRegion();
                    if (mod.file.isDirectory())
                        Logger.warn("Folder mods can't load sprites yet");
                    else {
                        URI resURI = new URI("jar:" + fileHandle.file().toURI().toString());
                        ZipFile zipFile;

                        if (resURI.getScheme().equals("jar")) {
                            zipFile = new ZipFile(mod.file);
                            Enumeration<? extends ZipEntry> entries = zipFile.entries();
                            Array<ZipEntry> sprites = new Array<>();
                            Array<ZipEntry> bundles = new Array<>();
                            while (entries.hasMoreElements()) {
                                ZipEntry entry = entries.nextElement();
                                if (entry.getName().startsWith("resources/sprites") && entry.getName().endsWith(".png"))
                                    sprites.add(entry);
                                else if (entry.getName().startsWith("resources/bundles") && entry.getName().endsWith(".properties"))
                                    bundles.add(entry);
                            }

                            if (sprites.size > 0) SpriteLoader.loadSprites(mod, sprites, zipFile);
                            if (bundles.size > 0) BundleLoader.loadBundles(mod, bundles, zipFile);

                            zipFile.close();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (mod.modInfo.dependencies != null) {
                for (String dep : mod.modInfo.dependencies) {
                    if (dependencies.contains(dep, false)) continue;
                    dependencies.add(dep);
                }
            }

            modContainer.addMod(mod);
        }

        if (dependencies.size >= 0) {
            Array<String> missingDeps = new Array<>();
            Array<String> modUUIDs = getModUUIDs();

            for (String dep : dependencies) {
                if (!modUUIDs.contains(dep, false)) {
                    missingDeps.add(dep);
                }
            }

            if (missingDeps.size > 1) Logger.err("YOU ARE MISSING DEPENDENCIES");
            else if (missingDeps.size == 1) Logger.err("YOU ARE MISSING A DEPENDENCY");
            if (missingDeps.size >= 1) {
                for (String s : missingDeps) {
                    Logger.warn("- {0}", Base64Coder.decodeString(s));
                }

                Logger.warn("Not loading mods.");
                return;
            }
        }

        Platform.instance.addURLs(urls.toArray(new URL[]{}));

        modClasses = Platform.instance.getClasses();

        if (modClasses == null)
            modClasses = new Class[]{};

        for (Class<?> cl : modClasses) {
            try {
                Method preInit = null;
                for (Method m : cl.getDeclaredMethods()) {
                    if (!m.isAnnotationPresent(Annotations.MMLEvent.class)) continue;
                    if (m.getAnnotation(Annotations.MMLEvent.class).event() == Annotations.Event.preinit) {
                        preInit = m;
                        break;
                    }
                }

                if (preInit == null) continue;

                try {
                    preInit.invoke(null);
                } catch (Exception e) {
                    Logger.err("A MOD FAILED TO PREINIT, THIS MIGHT CAUSE ISSUES, REPORT THIS TO THE MOD DEVELOPER");
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void modInit() {
        for (Class<?> mod : modClasses) {
            Method init = null;
            for (Method m : mod.getDeclaredMethods()) {
                if (!m.isAnnotationPresent(Annotations.MMLEvent.class)) continue;
                if (m.getAnnotation(Annotations.MMLEvent.class).event() == Annotations.Event.init) {
                    init = m;
                    break;
                }
            }

            if (init == null) continue;

            try {
                init.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void modPostInit() {
        for (Class<?> mod : modClasses) {
            Method postInit = null;
            for (Method m : mod.getDeclaredMethods()) {
                if (!m.isAnnotationPresent(Annotations.MMLEvent.class)) continue;
                if (m.getAnnotation(Annotations.MMLEvent.class).event() == Annotations.Event.postinit) {
                    postInit = m;
                    break;
                }
            }

            if (postInit == null)
                continue;

            try {
                postInit.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (FileHandle script : scriptFiles) {
            if (!enabledScripts.contains(script.nameWithoutExtension(), false)) continue;
            Object result = Platform.instance.runScript(script.file());
            if (result instanceof Exception) {
                String err = String.format("Error in %s\n", script.nameWithoutExtension()) + result.toString() +
                        "\n  at " + ((Exception) result).getStackTrace()[0];
                Vars.ui.crashDialog.setLog(err, false);
                Vars.ui.crashDialog.show();

                //Disable script.
                setScriptState(script.nameWithoutExtension(), false);
            }
        }
    }

    public Mod getMod(String uuid) {
        for (Mod m : modContainer.getMods()) {
            if (m.uuid.equals(uuid)) return m;
        }

        return null;
    }

    public boolean addModToContainer(Mod mod) {
        return modContainer.addMod(mod);
    }

    public void addScriptToContainer(FileHandle script) {
        scriptFiles.add(script);
    }

    public boolean isModEnabled(String uuid) {
        return enabledMods.contains(uuid, false);
    }

    public boolean isScriptEnabled(String name) {
        return enabledScripts.contains(name, false);
    }

    public boolean setScriptState(String name, Boolean shouldBeEnabled) {
        if (!doesScriptExist(name))
            throw new IllegalArgumentException("Code should check if script exists before enabling/disabling");

        if ((enabledScripts.contains(name, false) && shouldBeEnabled)
        || (!enabledScripts.contains(name, false) && !shouldBeEnabled))
            return false;
        else {
            if(shouldBeEnabled)
                enabledScripts.add(name);
            else
                enabledScripts.removeValue(name, false);
        }
        return false;
    }

    public boolean enableMod(String uuid) {
        if (doesModExist(uuid)) return false;
        if (!enabledMods.contains(uuid, false)) {
            enabledMods.add(uuid);
            return true;
        }
        return false;
    }

    public boolean disableMod(String uuid) {
        if (doesModExist(uuid)) return false;
        if (enabledMods.contains(uuid, false)) {
            enabledMods.removeValue(uuid, false);
            return true;
        }
        return false;
    }

    public boolean doesModExist(String uuid) {
        for (Mod m : getMods()) {
            if (m.uuid.equals(uuid)) return true;
        }
        return false;
    }

    public boolean doesScriptExist(String name) {
        for (FileHandle f : getScripts()) {
            if (f.nameWithoutExtension().equals(name)) return true;
        }
        return false;
    }

    public Array<Mod> getMods() {
        return modContainer.getMods();
    }

    public Array<FileHandle> getScripts() {
        return new Array<>(scriptFiles);
    }

    public Array<Mod> getEnabledMods() {
        Array<Mod> mods = new Array<>();
        for (Mod m : getMods()) {
            if (m.enabled)
                mods.add(m);
        }
        return mods;
    }

    public Array<String> getModUUIDs() {
        return new Array<>(modContainer.getModUUIDs());
    }

    public Array<String> getEnabledModUUIDs() {
        Array<String> uuids = new Array<>();
        for (Mod m : getMods()) {
            if (m.enabled)
                uuids.add(m.uuid);
        }
        return uuids;
    }

    public void delmod(Mod mod) {
        modContainer.delMod(mod);
    }

    public void delScript(FileHandle script) {
        for (FileHandle fileHandle : Vars.modDirectory.list()) {
            if (fileHandle.nameWithoutExtension().equals(script.nameWithoutExtension()))
                fileHandle.delete();
        }
        scriptFiles.removeValue(script, false);
    }

    public void swapModOrder(Mod m, String dir) {
        try {
            if (!modContainer.getMods().contains(m, false))
                return;
            int ind = modContainer.getMods().indexOf(m, false);
            modContainer.getMods().swap(ind, dir.equals("up") ? ind - 1 : dir.equals("down") ? ind + 1 : ind);
        } catch (Exception ignored) {
        }
    }

    public boolean areModsEnabled(boolean allowClientMods) {
        if(allowClientMods) {
            for (String s:enabledMods) {
                Mod m = getMod(s);
                if(!m.isClientSided)
                    return true;
            }

            return !enabledScripts.isEmpty();
        }

        return (!enabledMods.isEmpty() || !enabledScripts.isEmpty());
    }
}